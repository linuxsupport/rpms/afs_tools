Summary: afs_admin tool with runtime dependencies into AFS
Name: afs_tools
Version: 2.2
Release: 1%{?dist}
Source: %{name}-%{version}.tgz
License: GPL
Group: Applications/System
Requires: afs_tools_standalone
Requires: /usr/bin/arc
Requires: openafs-client
# console DB being pulled in?
Requires: perl-DBI
BuildRequires: /usr/bin/pod2man openafs-client
Packager: Arne Wiebalck <arne.wiebalck@cern.ch>
BuildRoot: %{_tmppath}/%{name}-root
BuildArch: noarch

%package -n afs_tools_standalone
Summary: AFS related tools
Requires: cern-wrappers
Requires: /usr/bin/klist /usr/bin/aklog

%description
tools around AFS management tasks with runtime dependencies on AFS:
     * afs_admin : administration tool for project space administrators (link)

%description -n afs_tools_standalone
This is a set of tools around AFS management tasks:
     * alifetime : report the lifetime of a CERN AFS token (in seconds)
     * k5reauth  : periodically refresh Kerberos5 and AFS credentials
     * afind     : AFS-volume-aware "find" version
     * cern_setserverprefs : tell CERN clients to not accidentally contacts
                   remote (CERN) servers

%prep
%setup -q -c -n %{name}-%{version}

%install
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1
/usr/bin/pod2man src/k5reauth src/k5reauth.1
/usr/bin/pod2man src/alifetime src/alifetime.1
install -m 644 src/*.1 $RPM_BUILD_ROOT%{_mandir}/man1/
rm src/*.1
install -m 755 src/*   $RPM_BUILD_ROOT%{_bindir}
ln -s  /afs/cern.ch/system/common/usr/local/etc/afs_admin $RPM_BUILD_ROOT%{_bindir}/

%clean
rm -rf $RPM_BUILD_ROOT

%files -n afs_tools_standalone
%defattr(-,root,root)
%{_bindir}/afind
%{_mandir}/man1/afind.1*
%{_bindir}/alifetime
%{_mandir}/man1/alifetime.1*
%{_bindir}/k5reauth
%{_mandir}/man1/k5reauth.1*
%{_bindir}/cern_setserverprefs.pl

%files
%defattr(-,root,root)
%{_bindir}/afs_admin
%{_mandir}/man1/afs_admin.1*


%changelog
* Fri Jan 24 2020 Ben Morrice <ben.morrice@cern.ch> - 2.2-1
- package for C8

* Mon Jun 20 2015 Jan Iven <jan.iven@cern.ch> 2.2-0.cern
- added several new options (--size, --iname, --mtime, --maxdepth) to "afind"

* Wed Feb 25 2015 Jan Iven <jan.iven@cern.ch> 2.1-2.cern
- split into standalone and AFS-dependent
- k5reauth is now a standalone script, manpage is auto-generated
- afs_admin is just a symlink into AFS, requires local 'arc' and 'openafs-client'
- several fixes to afind
- add cern_setserverprefs

* Thu Oct 10 2013 Arne Wiebalck <arne.wiebalck@cern.ch> 2.00-2.cern
- Added missing dependency (thanks to Samuele Kaplun for reporting this).

* Wed Oct 26 2011 Arne Wiebalck <arne.wiebalck@cern.ch> 2.00-1.cern
- Apparently, the tokens output can vary. Relaxed corresponding regexp.

* Wed Oct 26 2011 Arne Wiebalck <arne.wiebalck@cern.ch> 2.00-0.cern
- SLC6 cleanup: removed ksu, aexec, acopy

* Wed Apr 27 2011 Arne Wiebalck <arne.wiebalck@cern.ch> 1.07-3.cern
- updated documentation on how to create keytabs

* Wed Jul 1 2009 Arne Wiebalck <arne.wiebalck@cern.ch> 1.07-2.cern
- added -x option and some more documentation

* Thu May 13 2009 Arne Wiebalck <arne.wiebalck@cern.ch> 1.07-1.cern
- added k5reauth
- added deprecation warning to aexec, reauth and ksu

* Tue Jan 29 2008 bja
- new base with old patches integrated
- remove ksu
- fix some bugs

* Mon Jun 20 2005  Jan Iven <jan.iven@cern.ch> 1.06-8.cern
- looks also for "reauth.pl", and check for -x
- install man pages into correct location

* Tue Feb  1 2005  Andras Horvath <Andras.Horvath@cern.ch>
- minor fix to afs_admin

* Tue Jul  6 2004  Jan Iven <jan.iven@cern.ch>
- Import from ASIS, fixed pathes
